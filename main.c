#include <stdio.h>

int main(void) {
    //BIRTH is a constant, and doesn't change
    const int BIRTH = 1776;
    const int LEAPYRDAYS = 366; //Is fixed to account for leap years
    const int STANDARDYRDAYS = 365; //Is fixed to 365 and with no account for leap years.
    int standardYearsDays = 0;
    int leapYearsDays = 0;
    int currentYear = 2020;//Current year, will change, but is currently fixed.
    //Does not account for leap year in calculation.
    int estimatedResult = (currentYear - BIRTH) * STANDARDYRDAYS; 
    
    printf("The United States is approximately %d days old.\n", estimatedResult);
    //For loop to count years from US Birth, to current.
    for(int iterator = 1776; iterator <= currentYear; iterator++){
        /*Logic check to find leap years. Modulus check of the
          iterator, for any year evenly diviable by 4. */
        if((iterator % 4) == 0){
            //Totals each iteration. 
            leapYearsDays = leapYearsDays + LEAPYRDAYS;
        }else
        {
            //Same as above, collecting totals.
            standardYearsDays = standardYearsDays + STANDARDYRDAYS;
        }          
    }
    //Final sum of leap year days, and standard year days. 
    int totalDays = leapYearsDays + standardYearsDays;
    printf("Accounting for leap years, Total Days: %d", totalDays);
    return 0;
}